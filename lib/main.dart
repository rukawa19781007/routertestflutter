import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:routerutilitytest/authentication_bloc/authentication_bloc.dart';
import 'package:routerutilitytest/authentication_bloc/authentication_state.dart';
import 'package:routerutilitytest/firebase/user_repository.dart';
import 'package:routerutilitytest/login/login_page.dart';
import 'package:routerutilitytest/simple_bloc_delegate.dart';
import 'package:bloc/bloc.dart';
import 'package:routerutilitytest/home_page.dart';
import 'utils/preference_utils.dart';
import 'splash_screen/splash_screen.dart';
import 'dart:developer';

main() {
  Bloc.observer = SimpleBlocDelegate();
  runApp(new MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final UserRepository _userRepository = UserRepository();
  AuthenticationBloc _authenticationBloc;

  @override
  void initState() {
    _authenticationBloc = AuthenticationBloc(userRepository: _userRepository);
    PreferenceUtils.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (BuildContext context) => _authenticationBloc,
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            home: BlocBuilder(
              cubit: _authenticationBloc,
              builder: (context, state) {
                if (state is Authenticated) {
                  print('main show Authentication state : $state');
                  return HomePage();
                } else if (state is Unauthenticated) {
                  print('main show Authentication state : $state');
                  return LoginPage(
                    userRepository: _userRepository,
                  );
                }
                print('main show final Authentication state : $state');
                return SplashScreen(userRepository: _userRepository);
              },
            )));
  }

  @override
  void dispose() {
    _authenticationBloc.close();
    super.dispose();
  }
}
