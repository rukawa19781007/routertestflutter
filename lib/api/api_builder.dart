import 'package:equatable/equatable.dart';

class ApiBuilder extends Equatable {
  final bool isSecure;
  final String hostName;
  final String port;
  static String _baseUrl;
  const ApiBuilder({
    this.isSecure,
    this.hostName,
    this.port,
  });

  @override
  List<Object> get props => [isSecure, hostName, port];

  String getDeviceLogin() {
    return _baseUrl = (isSecure ? "https" : "http") +
        '://' +
        hostName +
        ':' +
        port +
        DEVICES_LOGIN;
  }

  String getSystemInformation(){
    return _baseUrl = (isSecure ? "https" : "http") +
        '://' +
        hostName +
        ':' +
        port +
        SYSTEM_INFORMATION;
  }
}
