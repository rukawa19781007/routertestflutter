import 'package:equatable/equatable.dart';

class SysInformation extends Equatable {
  final String name;
  final String product_name;
  final String desc_support;
  final String hardware_revision;
  final String product_code;
  final String serial;
  final String version;
  final String modem_link;
  final String modem_package;
  final String mvpn_version;
  final String company;
  final String legal;
  final String uuid;

  @override
  List<Object> get props => [
        name,
        product_name,
        desc_support,
        hardware_revision,
        product_code,
        serial,
        version,
        modem_link,
        modem_package,
        mvpn_version,
        company,
        legal,
        uuid
      ];

  const SysInformation(
      {this.name,
      this.product_name,
      this.desc_support,
      this.hardware_revision,
      this.product_code,
      this.serial,
      this.version,
      this.modem_link,
      this.modem_package,
      this.mvpn_version,
      this.company,
      this.legal,
      this.uuid});

  factory SysInformation.fromJson(Map<String, dynamic> json) {
    return SysInformation(
      name: json['name'],
      product_name: json['product_name'],
      desc_support: json['desc_support'],
      hardware_revision: json['hardware_revision'],
      product_code: json['product_code'],
      serial: json['serial'],
      version: json['version'],
      modem_link: json['modem_link'],
      modem_package: json['modem_package'],
      mvpn_version: json['mvpn_version'],
      company: json['company'],
      legal: json['legal'],
      uuid: json['uuid']
    );
  }

  @override
  String toString() =>
      'SysInformation { name: $name , product_name: $product_name , desc_support: $desc_support , hardware_revision: $hardware_revision , product_code: $product_code , serial: $serial , version: $version , modem_link: $modem_link , modem_package: $modem_package , mvpn_version: $mvpn_version , company: $company , legal: $legal, uuid: $uuid}';
}
