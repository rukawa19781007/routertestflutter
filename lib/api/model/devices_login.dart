import 'package:equatable/equatable.dart';

class DevicesLogin extends Equatable {
  final String stat;
  final String response;

  const DevicesLogin({this.stat, this.response});

  @override
  List<Object> get props => [stat, response];

  factory DevicesLogin.fromJson(Map<String, dynamic> json) {
    return DevicesLogin(
      stat: json['stat'],
      response: json['response'],
    );
  }

  @override
  String toString() => 'DevicesLogin { stat: $stat , response: $response }';
}

class Permission extends Equatable {
  final int GET;
  final int POST;

  const Permission({this.GET, this.POST});

  factory Permission.fromJson(Map<String, dynamic> json) {
    return Permission(
      GET: json["GET"] as int,
      POST: json["POST"] as int,
    );
  }

  @override
  List<Object> get props => [GET, POST];

  @override
  String toString() => 'Permission { GET: $GET , POST: $POST}';
}
