import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LoginEvent extends Equatable {
  LoginEvent([List props = const []]) : super();
}

class EmailChanged extends LoginEvent {
  final String email;

  EmailChanged({@required this.email}) : super([email]);

  @override
  List<Object> get props => throw UnimplementedError();

  @override
  String toString() => 'EmailChanged { email :$email}';
}

class PasswordChanged extends LoginEvent {
  final String password;

  PasswordChanged({@required this.password}) : super([password]);

  @override
  List<Object> get props => throw UnimplementedError();

  @override
  String toString() => 'PasswordChanged { password :$password}';
}

class LoginWithGooglePressed extends LoginEvent {
  @override
  List<Object> get props => throw UnimplementedError();

  @override
  String toString() => 'LoginWithGooglePressed';
}

class LoginWithCredentialsPressed extends LoginEvent {
  final String email;
  final String password;

  LoginWithCredentialsPressed({@required this.email, @required this.password})
      : super([email, password]);

  @override
  List<Object> get props =>throw UnimplementedError();

  @override
  String toString() {
    return 'LoginWithCredentialsPressed { email: $email, password: $password}';
  }
}
