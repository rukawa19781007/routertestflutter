import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:routerutilitytest/utils/preference_utils.dart';
import 'package:routerutilitytest/authentication_bloc/authentication_bloc.dart';
import 'package:routerutilitytest/authentication_bloc/authentication_event.dart';
import 'login.dart';
import 'package:routerutilitytest/firebase/user_repository.dart';
import 'package:routerutilitytest/login/bloc/login_bloc.dart';

class LoginForm extends StatefulWidget {
  final UserRepository _userRepository;

  LoginForm({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  LoginBloc _loginBloc;
  bool _rememberAccount;

  UserRepository get _userRepository => widget._userRepository;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
    _loadCheckbox();
    _loadAccount();
    super.initState();
  }

  _loadCheckbox() {
    setState(() {
      _rememberAccount = PreferenceUtils.getBool('remember_account') ?? false;
    });
  }

  _saveCheckout(value) {
    PreferenceUtils.setBool('remember_account', value);
  }

  _loadAccount() {
    setState(() {
      _emailController.text = PreferenceUtils.getString('account') ?? "";
    });
  }

  _saveAccount(account) {
    PreferenceUtils.setString('account', account);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
        listener: (BuildContext context, LoginState state) {
      if (state.isFailure) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[Text('Login Failure'), Icon(Icons.error)],
            ),
            backgroundColor: Colors.red,
          ));
      }
      if (state.isSubmitting) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Logging In...'),
                CircularProgressIndicator()
              ],
            ),
          ));
      }
      if (state.isSuccess) {
        if (_rememberAccount) _saveAccount(_emailController.text);
        BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
      }
    }, child: BlocBuilder<LoginBloc, LoginState>(
      builder: (BuildContext context, LoginState state) {
        return Padding(
          padding: EdgeInsets.all(20.0),
          child: Form(
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20.0),
                  child: Image.asset(
                    'assets/images/ic_launcher_foreground.png',
                    height: 200,
                  ),
                ),
                TextFormField(
                  controller: _emailController,
                  decoration: InputDecoration(
                      icon: Icon(Icons.email), labelText: 'Email'),
                  autovalidate: true,
                  autocorrect: false,
                  validator: (_) {
                    return !state.isEmailValid ? 'Invalid Email' : null;
                  },
                ),
                TextFormField(
                  controller: _passwordController,
                  decoration: InputDecoration(
                      icon: Icon(Icons.lock), labelText: 'Password'),
                  obscureText: true,
                  autovalidate: true,
                  autocorrect: false,
                  validator: (_) {
                    return !state.isPasswordValid ? 'Invalid Password' : null;
                  },
                ),
                CheckboxListTile(
                  value: _rememberAccount ?? false,
                  activeColor: Color(0xffffa200),
                  onChanged: (value) {
                    setState(() {
                      _rememberAccount = value;
                    });
                    _saveCheckout(value);
                  },
                  title: Text('Remember my account'),
                  controlAffinity: ListTileControlAffinity.leading,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      LoginButton(
                          onPressed: isLoginButtonEnabled(state)
                              ? _onFormSubmitted
                              : null),
                      GoogleSignInButton(
                        onPressed: () =>
                            _loginBloc.add(LoginWithGooglePressed()),
                      ),
                      CreateAccountButton(
                        userRepository: _userRepository,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    ));
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _loginBloc.add(EmailChanged(email: _emailController.text));
  }

  void _onPasswordChanged() {
    _loginBloc.add(PasswordChanged(password: _passwordController.text));
  }

  void _onFormSubmitted() {
    _loginBloc.add(LoginWithCredentialsPressed(
        email: _emailController.text, password: _passwordController.text));
  }
}
