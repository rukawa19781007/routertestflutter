import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:routerutilitytest/utils/device_profile.dart';
import 'package:routerutilitytest/device_login/bloc/bloc.dart';
import 'package:uuid/uuid.dart';
import 'utils/preference_utils.dart';

class AddDevice extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AddDeviceState();
}

class _AddDeviceState extends State<AddDevice> {
  bool isSecure = true;
  bool isSystemStartup = true;
  bool isWANupdown = true;
  bool isSFupdown = true;
  final TextEditingController _deviceNameController = TextEditingController();
  final TextEditingController _ipDomainController = TextEditingController();
  final TextEditingController _portController = TextEditingController();
  final TextEditingController _accountController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  onSwitchIsSecure(bool secure) {
    setState(() {
      isSecure = secure;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff0f0f0),
      appBar: AppBar(
        backgroundColor: Color(0xffffa200),
        leading: IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop()),
        title: Text(
          'AddDevice',
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.save),
              tooltip: 'Save',
              onPressed: () {
                List<DeviceProfile> profileList;
                String prefs = PreferenceUtils.getString('DeviceProfile') != null ? PreferenceUtils.getString('DeviceProfile') : '';
                print('add device save show prefs : ' + prefs);
                if (prefs.isNotEmpty) {
                  print('prefs != null');
                  profileList = DeviceProfile.decodeDeviceProfile(prefs);
                } else {
                  profileList = new List<DeviceProfile>();
                }
                profileList.add(DeviceProfile(
                  profileName: '',
                  hostName: _ipDomainController.text,
                  isSecureConnection: isSecure,
                  savedUsername: _accountController.text,
                  savedEncryptedPassword: _passwordController.text,
                  port: int.parse(_portController.text),
                  isPushNotificationOn: false,
                  isPushNotification1On: false,
                  isPushNotification2On: false,
                  isPushNotification3On: false,
                  uuid: Uuid().v4()
                ));
                print('show profileList : ' + profileList.toString());
                PreferenceUtils.setString('DeviceProfile',
                    DeviceProfile.encodeDeviceProfile(profileList));
                Navigator.pop(context);
              })
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: AnimationLimiter(
          child: ListView(
              children: AnimationConfiguration.toStaggeredList(
            duration: const Duration(milliseconds: 375),
            childAnimationBuilder: (widget) => SlideAnimation(
              verticalOffset: 50.0,
              child: FadeInAnimation(
                child: widget,
              ),
            ),
            children: childrenList(),
          )),
        ),
      ),
    );
  }

  List<Widget> childrenList() {
    List<Widget> child = [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('設備名稱'),
          SizedBox(width: 20),
          Expanded(
            child: TextField(
              decoration: InputDecoration(hintText: '自動檢測'),
              controller: _deviceNameController,
            ),
          ),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('主機名'),
          SizedBox(width: 20),
          Expanded(
            child: TextField(
              decoration: InputDecoration(hintText: '必要'),
              controller: _ipDomainController,
            ),
          ),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('安全連接'),
          SizedBox(width: 20),
          Switch.adaptive(
            activeColor: Color(0xffffa200),
              value: isSecure,
              onChanged: (secure) {
                onSwitchIsSecure(secure);
              })
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('端口(Port)號'),
          SizedBox(width: 20),
          Expanded(
            child: TextField(
              decoration: InputDecoration(hintText: isSecure ? '443' : '80'),
              controller: _portController,
            ),
          ),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('用戶名'),
          SizedBox(width: 20),
          Expanded(
            child: TextField(
              decoration: InputDecoration(hintText: '每次都詢問'),
              controller: _accountController,
            ),
          ),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('密碼'),
          SizedBox(width: 20),
          Expanded(
            child: TextField(
              decoration: InputDecoration(hintText: '每次都詢問'),
              controller: _passwordController,
            ),
          ),
        ],
      ),
      SizedBox(height: 30),
      Text(
        '想接收的遠端通知',
        style: TextStyle(color: Colors.grey),
      ),
      CheckboxListTile(
        activeColor: Color(0xffffa200),
        onChanged: (isCheck) {
          setState(() {
            isSystemStartup = isCheck;
          });
        },
        selected: true,
        value: isSystemStartup,
        title: Text("系統啟動"),
        controlAffinity: ListTileControlAffinity.leading,
      ),
      CheckboxListTile(
        activeColor: Color(0xffffa200),
        onChanged: (isCheck) {
          setState(() {
            isWANupdown = isCheck;
          });
        },
        selected: true,
        value: isWANupdown,
        title: Text("WAN上線/離線"),
        controlAffinity: ListTileControlAffinity.leading,
      ),
      CheckboxListTile(
        activeColor: Color(0xffffa200),
        onChanged: (isCheck) {
          setState(() {
            isSFupdown = isCheck;
          });
        },
        selected: true,
        value: isSFupdown,
        title: Text('PepVPN/SpeedFusion™ 在線/離線'),
        controlAffinity: ListTileControlAffinity.leading,
      ),
    ];
    return child;
  }

  @override
  void dispose() {
    _deviceNameController.dispose();
    _ipDomainController.dispose();
    _portController.dispose();
    _accountController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  _saveDeviceName(name) {
    PreferenceUtils.setString('deviceName', name);
  }

  _saveIpDomain(variable) {
    PreferenceUtils.setString('ipDomain', variable);
  }

  _savePort(port) {
    PreferenceUtils.setString('port', port);
  }

  _saveAccount(account) {
    PreferenceUtils.setString('deviceAccount', account);
  }

  _savePassword(password) {
    PreferenceUtils.setString('devicePassword', password);
  }
}
