import 'package:dio/adapter.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:routerutilitytest/utils/device_profile.dart';
import 'package:routerutilitytest/utils/preference_utils.dart';
import 'package:routerutilitytest/add_device.dart';
import 'package:routerutilitytest/device_login/bloc/bloc.dart';
import 'api/model/sys_information.dart';
import 'authentication_bloc/authentication_bloc.dart';
import 'authentication_bloc/bloc.dart';
import 'dart:io';
import 'package:xml/xml.dart';
import 'package:dio/dio.dart';
import 'package:xml2json/xml2json.dart';

class HomePage extends StatefulWidget {
  @override
  _DevicesListState createState() => _DevicesListState();
}

class _DevicesListState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  Map<String, String> headers = {};

  @override
  void initState() {
    super.initState();
  }

  void updateCookie(Response response) {
    String rawCookie = response.headers.toString();
    print('show headers : ${response.headers.toString()}');
    if (rawCookie != null) {
      int index = rawCookie.indexOf(';');
      headers['cookie'] =
          (index == -1) ? rawCookie : rawCookie.substring(0, index);
    }
  }

  Stream<List<SysInformation>> getDeviceInformation() async* {
    List<SysInformation> list = new List<SysInformation>();
    String prefs = PreferenceUtils.getString('DeviceProfile');
    print('show prefs : ' + prefs);
    List<DeviceProfile> profiles = DeviceProfile.decodeDeviceProfile(prefs);
    print('show profiles : ' + profiles.toString());
    for (var element in profiles) {
      String defaultPort = element.isSecureConnection ? '443' : '80';
      String link = (element.isSecureConnection ? 'https://' : 'http://') +
          element.hostName +
          ':' +
          (element.port.toString() ?? defaultPort) +
          '/cgi-bin/MANGA/api.cgi';
      print('show link : ' + link);
      try {
        Dio dio = Dio();
        (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
            (client) {
          print('httpClientAdapter');
          client.badCertificateCallback =
              (X509Certificate cert, String host, int port) => true;
        };

        Map<String, dynamic> map = new Map();
        map['username'] = element.savedUsername;
        map['func'] = 'login';
        map['password'] = element.savedEncryptedPassword;

        Response response = await dio.post(link, queryParameters: map);
        print('show response : ${response.data.toString()}');
        updateCookie(response);
        if (response.statusCode == 200) {
          var data = jsonDecode(response.toString());
          print('statusCode==200 : $data');
          if (data['stat'] == 'ok') {
            print('its ok');
            String sysPath =
                (element.isSecureConnection ? 'https://' : 'http://') +
                    element.hostName +
                    ':' +
                    (element.port.toString() ?? defaultPort) +
                    '/cgi-bin/MANGA/data.cgi?option=sysinfo';
            dio.options.headers = {"cookie": response.headers['set-cookie']};
            print(
                'show headers : ' + response.headers['set-cookie'].toString());
            Response sysInfoResponse = await dio.get(sysPath);
            XmlDocument document = parse(sysInfoResponse.toString());
            print('show document : ${document.toString()}');
            print(
                'show XMLdocument : ${document.toXmlString(pretty: true, indent: '\t')}');
            Xml2Json xml2json = new Xml2Json();
            xml2json.parse(document.toString());
            var json = xml2json.toParker();
            Map<String, dynamic> responseData = jsonDecode(json);
            responseData['data']['sysinfo']['uuid'] = element.uuid;
            var sysData = responseData['data']['sysinfo'];
            print(responseData['data']['sysinfo']);
            SysInformation sysInformation = SysInformation.fromJson(sysData);
            list.add(sysInformation);
            print('show list : $list');
          } else {
            print('cannot parse xml');
          }
        } else {
          var data = response.data;
          print('statusCode not 200 : $data');
        }
      } catch (error) {
        print("Exception occured: $error");
      }
    }

    yield list;
  }

  Future<List<SysInformation>> getData() async {
    List<SysInformation> list = new List<SysInformation>();
    String link = "https://t4-mbx.mypep.link:443/cgi-bin/MANGA/api.cgi";
    Response response;
    Dio dio;
    try {
      dio = Dio();
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (client) {
        print('httpClientAdapter');
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
      };

      Map<String, dynamic> map = new Map();
      map['username'] = 'admin';
      map['func'] = 'login';
      map['password'] = 'Peplink2302';

      response = await dio.post(link, queryParameters: map);
      print('show response : ${response.data.toString()}');
      updateCookie(response);

      if (response.statusCode == 200) {
        var data = jsonDecode(response.toString());
        print('statusCode==200 : $data');
        if (data['stat'] == 'ok') {
          print('its ok');
          String sysPath =
              'https://t4-mbx.mypep.link:443/cgi-bin/MANGA/data.cgi?option=sysinfo';
          dio.options.headers = {"cookie": response.headers['set-cookie']};
          print('show headers : ' + response.headers['set-cookie'].toString());
          Response sysInfoResponse = await dio.get(sysPath);
          XmlDocument document = parse(sysInfoResponse.toString());
          print('show document : ${document.toString()}');
          print(
              'show XMLdocument : ${document.toXmlString(pretty: true, indent: '\t')}');
          Xml2Json xml2json = new Xml2Json();
          xml2json.parse(document.toString());
          var json = xml2json.toParker();
          final Map<String, dynamic> responseData = jsonDecode(json);
          var sysData = responseData['data']['sysinfo'];
          print(responseData['data']['sysinfo']);
          SysInformation sysInformation = SysInformation.fromJson(sysData);
          list.add(sysInformation);
          print('show list : $list');
        } else {
          print('cannot parse xml');
        }
      } else {
        var data = response.data;
        print('statusCode not 200 : $data');
      }
    } catch (error) {
      print("Exception occured: $error");
    }
    print("List Size: ${list.length}");
    return list;
  }

  Widget listViewWidget(List<SysInformation> loginState) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/images/ic_launcher_foreground.png"),
              fit: BoxFit.scaleDown)),
      child: ListView.builder(
          itemCount: loginState.length,
          padding: const EdgeInsets.all(2.0),
          itemBuilder: (context, position) {
            return Slidable(
              key: Key(loginState.toString()),
              actionPane: SlidableDrawerActionPane(),
              // 展開方式
              actionExtentRatio: 0.25,
              // 每個 action 大小佔全部百分比
              child: Card(
                color: Color(0xfff0f0f0),
                child: ListTile(
                  leading: Icon(Icons.router),
                  title: Text(
                    '${loginState[position].name}',
                    style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text(loginState[position].product_name),
                  isThreeLine: true,
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      VerticalDivider(
                        width: 24.0,
                        thickness: 0.3,
                        color: Colors.black,
                      ),
                      Icon(Icons.edit),
                    ],
                  ),
//                onTap: () => _onTapItem(context, loginState[position]),
                ),
              ),
              secondaryActions: <Widget>[
                IconSlideAction(
                  caption: 'Delete',
                  color: Colors.red,
                  icon: Icons.delete,
                  onTap: () {
                    setState(() {
                      String prefs = PreferenceUtils.getString('DeviceProfile') != null ? PreferenceUtils.getString('DeviceProfile') : '';
                      final List<DeviceProfile> profileList = DeviceProfile.decodeDeviceProfile(prefs);
                      profileList.removeWhere( (item) => item.uuid == loginState[position].uuid);
                      print('remove list after: $profileList');
                      PreferenceUtils.setString('DeviceProfile',
                          DeviceProfile.encodeDeviceProfile(profileList));
                      loginState.removeAt(position);
                    });
                  },
                ),
              ],
            );
          }),

      // 右邊展開事件選項
//
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xffffa200),
        title: Text(
          'Dashboard',
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
      ),
      body: StreamBuilder(
          stream: getDeviceInformation(),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.active:
              case ConnectionState.waiting:
                return Center(child: CircularProgressIndicator());
                break;
              case ConnectionState.done:
                if (snapshot.data == null) {
                  print('init data == null');
                  return Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                                "assets/images/ic_launcher_foreground.png"),
                            fit: BoxFit.scaleDown)),
                  );
                } else {
                  print('get data :' + snapshot.data.toString());
                  return listViewWidget(snapshot.data);
                }
            }
            print('unhandled situation');
            return null;
//            return snapshot.data != null
//                ? listViewWidget(snapshot.data)
//                : Center(child: CircularProgressIndicator());
          }),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Color(0xffffa200),
        onPressed: () {
//            BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut()),
//          BlocProvider.of<DeviceLoginBloc>(context).add(DeviceLogin());
//          controller.forward();
          Navigator.of(context)
              .push(
                MaterialPageRoute(builder: (context) => AddDevice()),
              )
              .then((value) => setState(() {}));
        },
        label: Text('Add Device'),
        icon: Icon(Icons.router),
      ),
    );
  }
}
