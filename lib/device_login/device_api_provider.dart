import 'dart:convert';
import 'dart:io';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:dio_retry/dio_retry.dart';
import 'package:routerutilitytest/Utils/preference_utils.dart';
import 'package:routerutilitytest/api/api_builder.dart';
import 'package:routerutilitytest/api/model/sys_information.dart';
import 'package:xml/xml.dart';
import 'package:xml2json/xml2json.dart';

class DeviceApiProvider {
  Dio _dio;
  static final DeviceApiProvider _instance = DeviceApiProvider._internal();

  factory DeviceApiProvider() => _instance;

  DeviceApiProvider._internal() {
    if (null == _dio) {
      _dio = new Dio()
        ..interceptors.add(RetryInterceptor(
            options: const RetryOptions(
              retries: 1, // Number of retries before a failure
              retryInterval:
                  const Duration(seconds: 1), // Interval between each retry
            ),
            dio: _dio));
      _dio.options.connectTimeout = 30000;

      (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (client) {
        print('httpClientAdapter');
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
      };
    }
  }

  Future<bool> loginDevices(
      {bool isSecure,
      String hostName,
      String port,
      String userName,
      String password}) async {
    String link = ApiBuilder(isSecure: isSecure, hostName: hostName, port: port)
            .getDeviceLogin();
    Response response;
    final String prefCookie = 'prefCookie';
    try {
      Map<String, dynamic> map = new Map();
      map['username'] = userName;
      map['func'] = 'login';
      map['password'] = password;
      response = await _dio.post(link, queryParameters: map);
      print('show response : ${response.data.toString()}');

      if (response.statusCode == 200) {
        var data = jsonDecode(response.toString());
        print('statusCode==200 : $data');
        if (data['stat'] == 'ok') {
          _dio.options.headers = {"cookie": response.headers['set-cookie']};
          PreferenceUtils.setString(
              prefCookie, response.headers['set-cookie'].toString());

          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (error) {
      print("Exception occured: $error");
      return false;
    }
  }

  Future<List<SysInformation>> systemInformation(
      {bool isSecure,
      String hostName,
      String port,
      String userName,
      String password}) async {
    List<SysInformation> list = new List<SysInformation>();
    String link =
        new ApiBuilder(isSecure: isSecure, hostName: hostName, port: port)
            .getSystemInformation();
    Response response;
    try {
      response = await _dio.get(link);
      XmlDocument document = parse(response.toString());
      print(
          'show XMLdocument : ${document.toXmlString(pretty: true, indent: '\t')}');
      Xml2Json xml2json = new Xml2Json();
      xml2json.parse(document.toString());
      var json = xml2json.toParker();
      final Map<String, dynamic> responseData = jsonDecode(json);
      var sysData = responseData['data']['sysinfo'];
      print(responseData['data']['sysinfo']);
      SysInformation sysInformation = SysInformation.fromJson(sysData);
      list.add(sysInformation);
      return list;
    } catch (error) {
      return error;
    }
  }
}
