import 'package:routerutilitytest/api/model/sys_information.dart';
import 'package:routerutilitytest/device_login/device_api_provider.dart';

class DeviceRepository {
  final _deviceApiProvider = DeviceApiProvider();

  Future<bool> loginDevices(
          {bool isSecure,
          String hostName,
          String port,
          String userName,
          String password}) =>
      _deviceApiProvider.loginDevices(
          isSecure: isSecure,
          hostName: hostName,
          port: port,
          userName: userName,
          password: password);

  Future<List<SysInformation>> systemInformation(
          {bool isSecure,
          String hostName,
          String port,
          String userName,
          String password}) =>
      _deviceApiProvider.systemInformation(
          isSecure: isSecure,
          hostName: hostName,
          port: port,
          userName: userName,
          password: password);
}
