import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:routerutilitytest/device_login/device_repository.dart';
import './bloc.dart';
import 'package:meta/meta.dart';

class DeviceLoginBloc extends Bloc<DeviceLoginEvent, DeviceLoginState> {
  final DeviceRepository _deviceRepository;

  DeviceLoginBloc({@required DeviceRepository deviceRepository})
      : assert(deviceRepository != null),
        _deviceRepository = deviceRepository, super(DeviceLoginState.empty());

  @override
  Stream<DeviceLoginState> mapEventToState(
    DeviceLoginEvent event,
  ) async* {
    if (event is DeviceLogin) {
      yield* _mapDeviceLoginToState();
    }
  }

  Stream<DeviceLoginState> _mapDeviceLoginToState(
      {bool isSecure,
      String hostName,
      String port,
      String userName,
      String password}) async* {
    yield DeviceLoginState.loading();
    try {
      final _loginSuccess = await _deviceRepository.loginDevices(
          isSecure: isSecure,
          hostName: hostName,
          port: port,
          userName: userName,
          password: password);
      yield DeviceLoginState.success();
    } catch (_) {
      yield DeviceLoginState.failure();
    }
  }
}
