import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class DeviceLoginState {
  final bool isSuccess;
  final bool isFailure;

  DeviceLoginState({
    @required this.isSuccess,
    @required this.isFailure,
  });

  factory DeviceLoginState.empty() {
    return DeviceLoginState(
      isSuccess: false,
      isFailure: false,
    );
  }

  factory DeviceLoginState.loading() {
    return DeviceLoginState(
      isSuccess: false,
      isFailure: false,
    );
  }

  factory DeviceLoginState.failure() {
    return DeviceLoginState(
      isSuccess: false,
      isFailure: true,
    );
  }

  factory DeviceLoginState.success() {
    return DeviceLoginState(
      isSuccess: true,
      isFailure: false,
    );
  }

  @override
  String toString() {
    return '''DeviceLoginState{
    isSuccess: $isSuccess,
    isFailure: $isFailure,
    }''';
  }
}
