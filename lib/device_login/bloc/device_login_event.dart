import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DeviceLoginEvent extends Equatable {
  DeviceLoginEvent([List props = const []]) :super();
}

class DeviceLogin extends DeviceLoginEvent {
  @override
  List<Object> get props => throw UnimplementedError();

  @override
  String toString() => 'DeviceLogin';

}