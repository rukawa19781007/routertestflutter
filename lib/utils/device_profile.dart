import 'package:equatable/equatable.dart';
import 'dart:convert';

class DeviceProfile extends Equatable {
  String profileName;
  String hostName;
  bool isSecureConnection;
  String savedUsername;
  String savedEncryptedPassword;
  int port;
  bool isPushNotificationOn;
  bool isPushNotification1On;
  bool isPushNotification2On;
  bool isPushNotification3On;
  String uuid;

  @override
  List<Object> get props => [
        profileName,
        hostName,
        isSecureConnection,
        savedUsername,
        savedEncryptedPassword,
        port,
        isPushNotificationOn,
        isPushNotification1On,
        isPushNotification2On,
        isPushNotification3On,
        uuid
      ];

  DeviceProfile(
      {this.profileName,
      this.hostName,
      this.isSecureConnection,
      this.savedUsername,
      this.savedEncryptedPassword,
      this.port,
      this.isPushNotificationOn,
      this.isPushNotification1On,
      this.isPushNotification2On,
      this.isPushNotification3On,
      this.uuid});

  factory DeviceProfile.fromJson(Map<String, dynamic> json) {
    return DeviceProfile(
      profileName: json['profileName'],
      hostName: json['hostName'],
      isSecureConnection: json['isSecureConnection'],
      savedUsername: json['savedUsername'],
      savedEncryptedPassword: json['savedEncryptedPassword'],
      port: json['port'],
      isPushNotificationOn: json['isPushNotificationOn'],
      isPushNotification1On: json['isPushNotification1On'],
      isPushNotification2On: json['isPushNotification2On'],
      isPushNotification3On: json['isPushNotification3On'],
      uuid: json['uuid']
    );
  }

  static Map<String, dynamic> toMap(DeviceProfile profile) => {
        'profileName': profile.profileName,
        'hostName': profile.hostName,
        'isSecureConnection': profile.isSecureConnection,
        'savedUsername': profile.savedUsername,
        'savedEncryptedPassword': profile.savedEncryptedPassword,
        'port': profile.port,
        'uuid': profile.uuid
      };

  static String encodeDeviceProfile(List<DeviceProfile> profiles) =>
      json.encode(
        profiles
            .map<Map<String, dynamic>>(
                (profile) => DeviceProfile.toMap(profile))
            .toList(),
      );

  static List<DeviceProfile> decodeDeviceProfile(String profiles) =>
      (json.decode(profiles) as List<dynamic>)
          .map<DeviceProfile>((item) => DeviceProfile.fromJson(item))
          .toList();

  @override
  String toString() =>
      'DeviceProfile { profileName: $profileName , hostName: $hostName , isSecureConnection: $isSecureConnection , savedUsername: $savedUsername , savedEncryptedPassword: $savedEncryptedPassword , port: $port, uuid: $uuid }';
}
